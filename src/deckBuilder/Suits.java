package deckBuilder;

public enum Suits {
    SPADE('\u2660'), DIAMOND('\u2666'), CLUB('\u2663'), HEART('\u2764');

    private char suit;

    Suits(char suit) {
        this.suit = suit;
    }

    public char getSuit() {
        return suit;
    }
}
