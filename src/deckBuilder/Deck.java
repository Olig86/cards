package deckBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Deck {

    private List<Card> cardList;

    public Deck() {
        cardList = new ArrayList<>();
        for (Ranks rank : Ranks.values()) {
            cardList.add(new Card(rank, Suits.CLUB));
            cardList.add(new Card(rank, Suits.DIAMOND));
            cardList.add(new Card(rank, Suits.HEART));
            cardList.add(new Card(rank, Suits.SPADE));
        }
        Collections.shuffle(cardList);
    }

    public List<Card> getCardList() {
        return cardList;
    }

    //    public void showDeck() {
//        for (Card card :cardList) {
//            System.out.println(card.toString());
//        }
//    }
}
