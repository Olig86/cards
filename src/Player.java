import deckBuilder.Card;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private String name;
    private List<Card> cards;
    private String hand ="";

    public Player(String name) {
        this.name = name;
        cards = new ArrayList<>();
    }

    public List<Card> getCards() {
        return cards;
    }

    public String getName() {
        return name;
    }

    public String showHand() {
        for (Card card :cards) {
            hand += (card.toString() + " ");
        }
        return hand;
    }
}
