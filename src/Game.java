import deckBuilder.Card;
import deckBuilder.Deck;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game {

    private Deck cardDeck;
    private List<Player> playerList;

    public Game(List<Player> playerList) {
        //Czy w konstruktorze mógłbym podawać imiona graczy, ale tak aby nie bylo z gory oznaczonej ilosci graczy?
        //mam na mysli Stringi w nieokreslonej ilosci zamiast Listy graczy
        cardDeck = new Deck();
        this.playerList = playerList;
    }

    public boolean dealCards() {
        if(cardDeck.getCardList().size() >= playerList.size()) {
            for (Player player : playerList) {
                List<Card> cardList = cardDeck.getCardList();
                Random random = new Random();
                int i = random.nextInt(cardList.size());
                player.getCards().add(cardList.get(i));
                cardList.remove(i);
            }
            return true;
        }
        return false;
    }

    public Deck getCardDeck() {
        return cardDeck;
    }
}
