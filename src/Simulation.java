import deckBuilder.Card;
import deckBuilder.Deck;
import deckBuilder.Ranks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Simulation {

    public static void main(String[] args) {

        Player Jan = new Player("Jan");
        Player Oli = new Player("Oli");

        List<Player> players = new ArrayList<>();
        players.add(Jan);
        players.add(Oli);

        Game game = new Game(players);
        game.dealCards();
        game.dealCards();
        game.dealCards();
        game.dealCards();
        game.dealCards();

        System.out.printf("%s cards: %s%n",
                Jan.getName(),
                Jan.showHand()
        );
        System.out.printf("%s cards: %s%n",
                Oli.getName(),
                Oli.showHand()
        );

        List<Card> cardList = game.getCardDeck().getCardList();
        System.out.printf("Remaining cards in deck(%s):%n",
                cardList.size());
        for (Card card : cardList) {
            System.out.printf("%s ",
                    card.toString());
        }


    }
}
